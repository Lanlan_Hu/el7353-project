#!/bin/bash

interface=$1
echo $(sudo tc qdisc replace dev $interface root netem delay 20ms)
echo "delay=20ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 100ms)
echo "delay=100ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 210ms)
echo "delay=210ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 240ms)
echo "delay=240ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 280ms)
echo "delay=280ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 290ms)
echo "delay=290ms"
sleep 80
echo $(sudo tc qdisc replace dev $interface root netem delay 300ms)
echo "delay=300ms"
sleep 120
exit
