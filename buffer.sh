#!/bin/bash

buffer_size=$1
echo $(sudo sysctl -w net.core.rmem_max=$buffer_size)
echo $(sudo sysctl -w net.core.rmem_default=$buffer_size)
echo $(sudo sysctl -w net.core.wmem_max=$buffer_size)
echo $(sudo sysctl -w net.core.wmem_default=$buffer_size)
exit
