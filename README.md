#Introduction
In this paper, we measured the end-to-end TCP throughput and UDP packet loss on GENI testbed
with TCP and a small fraction of UDP traffic mixed at a very small buffer. And we found that
there exists an “anomalous region” for buffering, in which UDP packet loss increases as the 
buffer size increases, while the degradation for UDP performance does not result in significant 
improvement in TCP throughput.

The original paper:

A. Vishwanath, V. Sivaraman and G. Rouskas, "Anomalous Loss Performance for Mixed Real-Time 
and TCP Traffic in Routers With Very Small Buffers," in IEEE/ACM Transactions on Networking,
vol. 19, no. 4, pp. 933-946, Aug. 2011.

http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5648399.

For a certain buffer size B, it takes 15 minutes to run the experiment and deal with the 
experiment results. As we need to implement experiment on B ranges from 1kB to 50kB, it will
take about 14 hours in total to finish the experiment and reproduce the figures.

#Background
In traditional view, the router needs enough buffers to store a round-trip-time worth of data 
to achieve fully utilization of output TCP link. However, in recent years, researchers have 
proposed that under certain conditions, few tens of packets of buffering are sufficient to 
realize fully utilization for TCP traffic. Although this solves the problem of large buffering,
it ignores the performance for real-time (UDP) traffic.

In this paper, we measured the end-to-end TCP throughput and UDP packet loss with UDP and TCP 
traffic mixed. And different from the commonly-held view that UDP packet loss falls 
monotonically as buffer size increases, we found that there exists an “anomalous region” for
buffering, in which UDP packet loss increases as the buffer size increases. Simultaneously,
contrary to our expectation, the degradation for UDP performance does not result in 
significant improvement in TCP throughput. To be more specific, TCP has already come to its
saturation throughput when the “anomalous region” occurs.

The existence of the "anomalous region" is of great importance. On the one hand, since 
real-time multimedia applications, such as online gaming and IPTV, account for a large 
proportion in the Internet, the influence of router buffer sizing on UDP traffic mixed with 
TCP traffic cannot be neglected. On the other hand, as the “anomalous region” exists, we 
should pay attention to balancing the performance of UDP and TCP traffic when setting router
buffer sizes in the range of very small buffers.

#Results
-1. We reproduce the plot of UDP packet loss performance for different values of B* with 
varying B.

![Fig19](http://ww1.sinaimg.cn/large/74311666jw1f3gt1fek9hj20ao073wfe.jpg)

(Fig.19. from 
http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5648399)

The experiment results on GENI:

![UDP](http://i2.buimg.com/b891d748dfc161d1.jpg)

The above figure shows the impact of B`*`,
on UDP loss performance on GENI testbed, and ranges of the “anomalous region” for each B*, 
in which the packet loss probability increases as buffer size increases, are listed in the
table:

![region](http://i4.piimg.com/a785b82b684d25c5.jpg)

Compared with the original figure, the experiment results on testbed show similar trends 
with analytical results. For each value of B*, UDP packet loss probability decreases as 
buffer size B increases at the beginning. Then it comes to the inflection point, at which 
UDP packet loss begins to increases as buffer size increases. The “anomalous regions” on 
testbed occur within [4kB, 24kB], roughly corresponds to the range of “anomalous region” in 
analytical model. 

The magnitude of testbed results is slightly different from analytical results. For the 
experiment results, the UDP packet loss probability decreases or increases within a small 
range, while the anomaly of analytical results is much more severe. The possible reason for 
this might be some other system parameters in testbed experiment that introduce fluctuations
and make the results less pronounced.

-2. We reproduce the plot of TCP throughput performance for different values of B* with 
varying B.

![Fig18](http://ww4.sinaimg.cn/large/74311666jw1f3gt4rf8qoj20bs085jsi.jpg)

(Fig.18. from 
http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5648399)

The experiment results on GENI:

![TCP](http://i3.buimg.com/686567c362abb6aa.jpg)

Compared with analytical results in the original figure, as B`*` increases, the difference 
between results on testbed and analytical saturation throughput (94Mbps) rises. The 
possible reason is using propagation delay on the bottleneck link to tune B`*`, and the 
saturation throughput on testbed is restricted within a smaller value as RTT increases. 
As we obtain the value of TCP throughput after running the script for 80 seconds, another 
possible explanation is that for larger B`*`, TCP needs more time to realize stabilization. 
In order to identify whether the TCP throughput is restricted by link delay or affected by 
time duration, we do a test with B*=14kB, B=40kB, runtime=600s, and the value of TCP 
throughput is obtained every 30 seconds. The result shows no significant improvement when 
time duration is larger, and the values merely fluctuate around mean TCP throughput. So 
the assumption that bottleneck link delay is the predominant restriction on TCP throughput 
justified.

-3. We extract the values of TCP throughput and UDP loss probability when B* is 7kB and 
create a new plot, then compare it with the analytical and simulation results.

![Fig10](http://i4.piimg.com/4a6914e05ad60d76.png)

(Analytical result. Fig.10. from 
http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5648399)

![Fig11](http://i4.piimg.com/581374371f0c3cb7.png)

(Simulation result. Fig.11 from 
http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5648399)

The experiment result on GENI testbed:

![GENI](http://i3.buimg.com/9cf267d32bce62e4.jpg)

For TCP throughput performance, the saturation throughput on testbed is restricted by 
bottleneck link delay, hence the results on testbed approaches to a smaller value than 
analytical and simulation results. 

For UDP packet loss probability, the “anomalous region” on GENI testbed is 10-24 kB, 
while the range is approximately 9-30 kB in analytical model and 13-27kB in simulation.
And despite the slight difference on magnitude, the testbed result displays similar trends 
with analytical and simulation results.
  
For the interaction between TCP and UDP traffic, we can find in all three figures, when
buffer size comes to the “anomalous region”, UDP packet loss begins to increases as buffer 
size increases while no significant improvement in TCP throughput occurs. 

#Run My Experiments

-1. Set up the topology

Use project_rspec.xml in the source to set up the topology like this:

![topo](http://7xrn7f.com1.z0.glb.clouddn.com/16-5-2/73991822.jpg)

Check in Jacks to make sure the bandwidth between s-tcp1 and r0, and the bandwidth between
s-tcp2 and r0 are set to be 600000(kbps) in both directions.

As there are links with bandwidth over 100Mbps, it's better to use ExoGeni or Apt to aggregate. 

-2. Set up the hosts.

On each source and destination hosts (s-udp, s-tcp1, s-tcp2, d-udp, d-udp1, d-udp2) install
d-itg.

`sudo apt-get install d-itg`

Set the propagation delay between s-udp and r0 to be 5ms. Run the command on s-udp, where
eth1 represents the interface on s-udp that connects to r0.

`sudo tc qdisc replace dev eth1 root netem delay 5ms`

Set the propagation delay between TCP access links to be randomly distributed in [1, 25]ms.
Run the command on s-tcp1 and s-tcp2, where eth1 represents the interface on s-tcp that 
connects to r0

`sudo tc qdisc replace dev eth1 root netem delay 13ms 12ms`

On each source host, ping its destination host to check the connection.

-3. Create script files.

1) On s-tcp1, use scrip1 in the source to create a file.

`vi script1`

2) On s-tcp2, use script2 in the source to create a file.

`vi script2`

3) On the router r0, use buffer.sh in the source to create a file. It is used to change UDP
buffer size in the router.

`vi buffer.sh`

Then make it executable.

`chmod a+x buffer.sh`

4) On the router r0, use delay.sh in the source to create a file. It is used to change delay 
on the bottleneck link, hence tune the value of B*.

`vi delay.sh`

Then make it executable.

`chmod a+x delay.sh`

The relationship between delay and B* is listed in the table below:

![delay](http://i2.piimg.com/3bcb2e832c119a60.jpg)

-4. Start sending flows.

1) On the router r0, set the buffer size in bytes. For example, if you want to run the 
experiment with B=5kB, run the command to set buffer size to be 5120 bytes

`./buffer.sh 5120`

In this experiment, the varying factor is buffer size B, ranges from 1 to 50kB.

2) On d-udp, d-tcp1, d-tcp2, start ITGRecv.

`ITGRecv`

3) On the router r0, set the bottleneck link delay. Run the command on r0, where eth4 
represents the interface on r0 that connects to r1.

`./delay.sh eth4`

This command would change the value of delay every 80 seconds, which means, the value of 
B* is changed every 80 seconds in the experiment.

4) Use ITGSend to send flows.

On s-udp, run the command to send a Poisson UDP flow with rate=5Mbps, runtime=600s.

`ITGSend -a d-udp -l send.log -x recv.log -E 3125 -e 200 -T UDP -t 600000`

On s-tcp1, run the command to send 50 Poisson TCP flows with rate=10Mbps for each flow. 

`ITGSend script1 -l send.log -x recv.log` 

On s-tcp2, run the command to send 50 Poisson TCP flows with rate=10Mbps for each flow.

`ITGSend script2 -l send.log -x recv.log`

Wait until ITGSend has finished on each source host.

Tips: Sometimes due to transmission delay, the information of finishing will not display in
the terminal immediately. In this case, you can check delay.sh on the router r0. The script
delay.sh is set to run for 600s, and if it has finished, you can use Ctrl+C in the source 
and destination hosts to shut down the traffic by force.


-5. Obtain experiment data.

1) On d-udp, run the command to obtain the number of lost packets in every 80 seconds and
store the results in pktloss.txt

`ITGDec recv.log -p 80000 pktloss.txt`

Then on s-udp, run the command to obtain the average packet rate from logfile.

`ITGDec recv.log`

2) On d-tcp1, run the command to obtain the average bitrate (in kbps) in every 80 seconds 
and store the results in bitrate1.txt

`ITGDec recv.log -b 80000 bitrate1.txt`

3) On d-tcp2, run the command to obtain the average bitrate (in kbps) in every 80 seconds 
and store the results in bitrate2.txt

`ITGDec recv.log -b 80000 bitrate2.txt`

-6. Deal with data

1) On local host, use `scp` command to download pktloss.txt, bitrate1.txt, and bitrate2.txt 
from destination hosts. And I would recommend Excel for output.

2) In bitrate1.txt, finds the last column "Aggregate Flow", and obtain the values of the first
seven rows. From top to the bottom, the value of each row represents the average bitrate
sending by s-tcp1 
for B*=1kB, 2kB, 5kB, 7kB, 10kB, 12kB, 14kB. We abort the last row in this column as some
flow has shut down so the value is not correct.

In bitrate2.txt, finds the first seven rows in the last column "Aggregate Flow".

Add the values obtained from bitrate1.txt with its corresponding value in bitrate2.txt. 
For example,

(Total bitrate for B*=1kB in kbps)=(first row in bitrate1)+(first row in bitrate2)

Then change the seven bitrates into Mbps to get TCP throughput.

(TCP throughput for B`*`=1kB)=(Total bitrate for B`*`=1kB)/1024

By doing this, we obtain the TCP throughput for seven different B* at a specific B.

3) In pktloss.txt, find the first seven rows in the column "Aggregate Flow".
From top to the bottom, the value of each row represents the number of lost packets
in 80 s for B`*`=1kB, 2kB, 5kB, 7kB, 10kB, 12kB, 14kB.
In the tests done before, we observed that even though UDP packet loss probability varies as
B`*` (delay) changes, the UDP traffic bitrate and packet rate stay relatively stable. 
Hence,we can use the average packet rate (packet/s) retrived from logfile to obtain UDP packet loss. 
For example,

(UDP packet loss probability for B`*`=1kB)
=(number of lost packets for B`*`=1kB)/((average packet rate)*80)

By doing this, we obtain the UDP packet loss for seven different B* at a specific B.

-7. Change the values of B from 1kB to 50kB. For each value of B, repeat the steps 1-6 to
get experiment data. Then create a plot for TCP throughput as a function of B with seven
different B`*`, create a plot for UDP packet loss as a function of B with seven different 
B`*.
At last, extract the values of TCP throughput and UDP packet loss with B`*`=7kB, and
create the third plot.

#Notes

For high link capacity, it's better to use ExoGeni or Apt to aggregate.

Due to the different aggregate you choose, the network condition in local machine, and other factors in the system, the data of reproducing experiments may not be exactly same with my experiment results, but it should display similar trends in TCP throughput and UDP packet loss.